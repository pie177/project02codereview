﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance = null;

    public AudioSource backgroundMusic;
    public AudioSource soundEffect1;
    public AudioSource soundEffect2;
    public AudioSource soundEffect3;
    public AudioSource soundEffect4;


    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
    }

    public void PlayBackGroundMusic(AudioClip sound)
    {
        backgroundMusic.clip = sound;
        backgroundMusic.Play();
    }

    public void PlaySoundEffect(AudioClip sound)
    {

        if (!soundEffect1.isPlaying)
        {
            soundEffect1.clip = sound;
            soundEffect1.Play();
        }
        else if (!soundEffect2.isPlaying)
        {
            soundEffect2.clip = sound;
            soundEffect2.Play();
        }
        else if (!soundEffect3.isPlaying)
        {
            soundEffect3.clip = sound;
            soundEffect3.Play();
        }
        else if (!soundEffect4.isPlaying)
        {
            soundEffect4.clip = sound;
            soundEffect4.Play();
        }

    }
}
