﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {


	public enum ItemType{
		Ammo, 
		Health,
		Coffee,
		Light,
		Water,
		Pills,
		Fire,
		Bolt,
		Clock,
		Car
	};

	public bool stackable;
	public Sprite itemImageSprite;
	//public string itemImageName;
	public ItemType typeOfItem;
	public bool canPickUp;

	void Start() {


		switch (typeOfItem) {
		case ItemType.Ammo:
			itemImageSprite = Ui_Controller.instance.itemTypeImageSources [0];
			break;
		case ItemType.Bolt:
			itemImageSprite = Ui_Controller.instance.itemTypeImageSources [8];
			break;
		case ItemType.Car:
			itemImageSprite = Ui_Controller.instance.itemTypeImageSources [10];
			break;
		case ItemType.Clock:
			itemImageSprite = Ui_Controller.instance.itemTypeImageSources [9];
			break;
		case ItemType.Coffee:
			itemImageSprite = Ui_Controller.instance.itemTypeImageSources [3];
			break;
		case ItemType.Fire:
			itemImageSprite = Ui_Controller.instance.itemTypeImageSources [7];
			break;
		case ItemType.Health:
			itemImageSprite = Ui_Controller.instance.itemTypeImageSources [1];
			break;
		case ItemType.Light:
			itemImageSprite = Ui_Controller.instance.itemTypeImageSources [4];
			break;
		case ItemType.Pills:
			itemImageSprite = Ui_Controller.instance.itemTypeImageSources [6];
			break;
		case ItemType.Water:
			itemImageSprite = Ui_Controller.instance.itemTypeImageSources [5];
			break;

		}
	}
}
