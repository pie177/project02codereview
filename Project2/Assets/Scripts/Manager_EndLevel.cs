﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Manager_EndLevel : MonoBehaviour
{
	public string loadLevel;
	public static int numEnemies;
	// Use this for initialization
	void Start () {
		GameObject[] enemies = GameObject.FindGameObjectsWithTag ("Enemy");
		if (enemies [0] != null) {
			numEnemies = enemies.Length;
		} else {
			numEnemies = 1;
		}
		Debug.Log (numEnemies);
	} 

	// Update is called once per frame
	void Update () {
		EndLevel ();
	}

	public void EndLevel(){
		if (numEnemies <= 0) {
			SceneManager.LoadScene (loadLevel);
			if (PointSystem.instance == null) {
				if (numEnemies <= 0) {
					NextLevel ();
				}
			} else if (PointSystem.instance != null) {
				if (PointSystem.instance.currentPoints >= PointSystem.instance.pointsToWin) {
					NextLevel ();
				}
			}


		}
	}

		public void NextLevel() {
		SceneManager.LoadScene (loadLevel);
		}
	}