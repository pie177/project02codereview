﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion_Field : MonoBehaviour {
	public float damagePoints;
	// Use this for initialization
	void Start () {
		Destroy (this.gameObject, 1f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnTriggerEnter(Collider other){
		if (other.CompareTag ("Enemy")) {
			other.gameObject.GetComponent<Enemy_Controller> ().enemyHealth -= damagePoints;
		}
	}
}
