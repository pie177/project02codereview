﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.SceneManagement;

public class SaveAndLoad : MonoBehaviour {

	public void SaveGame(){
		BinaryFormatter binaryFormat = new BinaryFormatter ();

		FileStream fileStreamer = File.Create (Application.persistentDataPath + "/saveFile.octo");
		GameSaveManager gameSaver = new GameSaveManager ();

		gameSaver.playerHealth = GameManager.playerHealth;
		gameSaver.playerLives = GameManager.playerLives;
		gameSaver.assaultAmmo = Gun_AssaultRifle.ammoCount;
		gameSaver.shotgunAmmo = Gun_Shotgun.ammoCount;
		gameSaver.bazookaAmmo = Gun_Bazooka.ammoCount;
		gameSaver.assaultAccess = Gun_Controller.assaultAccess;
		gameSaver.shotgunAccess = Gun_Controller.shotgunAccess;
		gameSaver.bazookaAccess = Gun_Controller.bazookaAccess;
		gameSaver.sceneName = SceneManager.GetActiveScene ().name;
		//gameSaver.items.AddRange(InventoryManager.items);
        //brings up null exception
		binaryFormat.Serialize (fileStreamer, gameSaver);
		fileStreamer.Close ();
	}
	public void LoadGame(){
		if (File.Exists (Application.persistentDataPath + "/saveFile.octo")) {
			BinaryFormatter binaryFormatter = new BinaryFormatter ();
			FileStream fileStreamer = File.Open (Application.persistentDataPath + "/saveFile.octo", FileMode.Open);
			GameSaveManager gameSaver = (GameSaveManager)binaryFormatter.Deserialize (fileStreamer);
			fileStreamer.Close ();

			GameManager.playerHealth = gameSaver.playerHealth;
			GameManager.playerLives = gameSaver.playerLives;
			Gun_AssaultRifle.ammoCount = gameSaver.assaultAmmo;
			Gun_Shotgun.ammoCount = gameSaver.shotgunAmmo;
			Gun_Bazooka.ammoCount = gameSaver.bazookaAmmo;
			Gun_Controller.assaultAccess = gameSaver.assaultAccess;
			Gun_Controller.shotgunAccess = gameSaver.shotgunAccess;
			Gun_Controller.bazookaAccess = gameSaver.bazookaAccess;
			//InventoryManager.items.AddRange(gameSaver.items);
			SceneManager.LoadScene (gameSaver.sceneName);
		}
	}
}
[System.Serializable]
class GameSaveManager{
	public int playerHealth;
	public int playerLives;
	public int assaultAmmo;
	public int shotgunAmmo;
	public int bazookaAmmo;
	public bool assaultAccess;
	public bool shotgunAccess;
	public bool bazookaAccess;
	public string sceneName;
    public List<GameObject> items;
}
